<!DOCTYPE html>
<html lang="en">

<head>

    <title>Sertifikat vocasia</title>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="author" content="" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link name="favicon" type="image/x-icon" href="#" rel="shortcut icon" />
    <link href='https://fonts.googleapis.com/css?family=Source Sans Pro' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Alex Brush' rel='stylesheet'>
    <link rel="stylesheet" href="https://use.typekit.net/sla7osm.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Niramit:wght@200&display=swap" rel="stylesheet">
    <style type="text/css" media="screen">
        @font-face {
            font-family: CenturyGothic;
            src: url();
        }

        .bg-certificate {
            background-image: url('cer.jpg');

            /* Full height */
            height: 950px;
            width: 1232.090px;
            /*	height: 100%;
			width: 100%;*/
            margin-left: auto;
            margin-right: auto;
            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .text_name {
            position: absolute;
            left: 0;
            right: 0;
            top: 370px;
            width: 900px;
            text-align: center;
            font-size: 40px;
            color: #4A4B4D;
            margin-left: auto;
            margin-right: auto;
            font-family: source-sans-pro, sans-serif;
            font-weight: 700;
            font-style: normal;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
        }

        .text_title {
            position: absolute;
            left: 0;
            right: 0;
            top: 490px;
            height: 130px;
            width: 855px;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            font-size: 40px;
            color: #4A4B4D;
            font-weight: 900 !important;
            margin-left: auto;
            margin-right: auto;
            font-family: 'source-sans-pro', sans-serif;
        }

        .text_date {
            position: absolute;
            left: 0;
            right: 0;
            top: 623px;
            width: 400px;
            text-align: center;
            font-size: 26px;
            color: #4A4B4D;
            margin-left: auto;
            margin-right: auto;
            font-family: 'Source Sans Pro';
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
        }

        /*		.ttd{
			position: absolute;
			left: 0;
			right: 0;
			padding-top: 532px;
			width: 600px;
			text-align: center;
			font-size: 40px;
			color: #4A4B4D;
			font-style: italic;
			margin-left: auto;
  			margin-right: auto;
			font-family: 'Alex Brush';
			margin-bottom: 0px;	
		}
		.name-ttd{
			position: absolute;
			left: 0;
			right: 0;
			padding-top: 590px;
			width: 600px;
			text-align: center;
			font-size: 13px;
			color: #E17226;
			font-style: italic;
			margin-left: auto;
  			margin-right: auto;
			font-family: 'Source Sans Pro';	
			font-weight: 600;
		}*/
        .id_courses {
            position: absolute;
            padding-top: 800px;
            width: 600px;
            padding-left: 120px;
            font-size: 12px;
            margin-left: auto;
            font-family: CenturyGothic;
            color: gray;
            font-weight: 600;
        }

        .link_courses {
            position: relative;
            padding-top: 820px;
            width: 100%;
            padding-left: 120px;
            color: grey;
            font-size: 12px;
            text-align: left;
            font-family: CenturyGothic;
            font-weight: 600;
        }

        .license {
            position: absolute;
            top: 795px;
            right: 150px;
            text-align: justify;
            direction: rtl;
            font-family: CenturyGothic;
            color: grey;
            float: right;
            width: 350px;
            font-size: 12px;
            font-weight: 600;
            line-height: 110%;
        }
    </style>
</head>

<body>

    <div class="bg-certificate" id="capture">

        <p class="text_name">
            Muhammad Irhas Alba'is
        </p>

        <p class="text_title">
            Kiat Menjadi Video Editor Menggunakan Smartphone
        </p>
        <p class="text_date">
            Jakarta,21 Desember 2021
        </p>
        <p class="id_courses">Certificate no: <b>
                3808302803820830
            </b></p>
        <p class="link_courses">Certificate url: <b>
                url sertif
            </b></p>
    </div>
    <div id="can" class="text-center">

    </div>
    <br>
    <div class="row">
        <div class="col-12 text-center" id="download">
            <a href="#" class="btn btn-info" id="btn-download">Download PNG</a>
            <a href="#" class="btn btn-info" id="btn-download-pdf">Download PDF</a>
        </div>
    </div>
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            html2canvas(document.querySelector("#capture")).then(canvas => {
                $('#can').append(canvas);
                $('#capture').hide();
            });
        });

        $('#btn-download').click(function (event) {
            var $canvas = document.querySelector("canvas");
            $canvas.toBlob(function (blob) {
                saveAs(blob, "certificate.png");
            });
        });

        $('#btn-download-pdf').click(function (event) {
            var $canvas = document.querySelector("canvas");
            var imgData = $canvas.toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF('l', 'mm', [325, 253]);;
            pdf.addImage(imgData, 'JPEG', 0, 3);
            pdf.save("certificate.pdf");
        });
    </script> -->
</body>

</html>