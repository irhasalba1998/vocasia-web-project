<?php

namespace App\Controllers\Frontend;

use App\Controllers\Frontend\FrontendController;
use CodeIgniter\I18n\Time;

class Home extends FrontendController
{
    protected $format = 'json';

    public function __construct()
    {
        helper(['jwt_helper', 'parse_date']);
    }
    public function get_all_courses()
    {
        if (!is_null($this->request->getVar('limit')) && !is_null($this->request->getVar('page'))) {
            $page = $this->request->getVar('page');
            $limit = $this->request->getVar('limit');
            $pagging_course = $this->pagging_course($page, $limit);
            return $this->respond(response_pagging($pagging_course['total_pages'], $pagging_course['data_course']));
        }
        if ($this->request->getVar('category')) {
            $slug_category = $this->request->getVar('category');
            $course_by_category = $this->model_course->get_course_by_category($slug_category);
            $data = array();
            foreach ($course_by_category as $key => $cbc) {

                $lesson = $this->model_course->get_lesson_duration(['course_id' => $cbc['id']]);
                $total_students = $this->model_enrol->get_count_enrols_courses($cbc['id']);
                $rating_review = $this->model_course->get_rating_courses($cbc['id']);
                if ($cbc['discount_price'] != 0) {
                    $get_discount_percent = ($cbc['discount_price'] / $cbc['price']) * 100;
                } elseif ($cbc['discount_price'] == 0) {
                    $get_discount_percent = 0;
                }
                $duration = $this->get_duration($lesson);
                $data[$key] = [
                    "id_course" => $cbc['id'],
                    "instructor_id" => $cbc["instructor_id"],
                    "title" =>  $cbc['title'],
                    "short_description" => $cbc['short_description'],
                    "price" => $cbc['price'],
                    "instructor_name" => $cbc['first_name'] . ' ' . $cbc['last_name'],
                    "discount_flag" => $cbc['discount_flag'],
                    "discount_price" => $cbc['discount_price'],
                    "thumbnail" => $this->model_course->get_thumbnail($cbc['id']),
                    "level_course" => $cbc['level_course'],
                    "total_lesson" => $cbc['total_lesson'],
                    "duration" => $duration,
                    "students" => $total_students,
                    "rating" => $rating_review,
                    "total_discount" => intval($get_discount_percent),
                    "foto_profile" => $this->model_users->get_foto_profile($cbc['instructor_id']),

                ];
            }
        } else if ($this->request->getVar('top_course')) {
            $top_course_param = $this->request->getVar('top_course');
            $check_top_course = $top_course_param == 1 ? 1 : 0;
            $top_course = $this->model_course->top_course($check_top_course);
            $data = array();
            foreach ($top_course as $key => $all_course) {
                $total_students = $this->model_enrol->get_count_enrols_courses($all_course['id']);
                $rating_review = $this->model_course->get_rating_courses($all_course['id']);
                if ($all_course['discount_price'] != 0) {
                    $get_discount_percent = ($all_course['discount_price'] / $all_course['price']) * 100;
                } elseif ($all_course['discount_price'] == 0) {
                    $get_discount_percent = 0;
                }
                $discount = intval($get_discount_percent);
                $data[$key] = [
                    "id_course" => $all_course['id'],
                    "instructor_id" => $all_course["instructor_id"],
                    "title" =>  $all_course['title'],
                    "price" => $all_course['price'],
                    "instructor_name" => $all_course['first_name'] . ' ' . $all_course['last_name'],
                    "discount_flag" => $all_course['discount_flag'],
                    "discount_price" => $all_course['discount_price'],
                    "thumbnail" => $this->model_course->get_thumbnail($all_course['id']),
                    "students" => $total_students,
                    "rating" => $rating_review,
                    "total_discount" => $discount,
                    "foto_profile" => $this->model_users->get_foto_profile($all_course['instructor_id']),
                    "top_course" => $all_course['is_top_course']

                ];
            }
            return $this->respond(get_response($data));
        } else if ($this->request->getVar('latest')) {
            $data_course_latest = $this->model_course->latest_course();
            $data_array = array();
            foreach ($data_course_latest as $key => $all_course) {
                $total_students = $this->model_enrol->get_count_enrols_courses($all_course['id']);
                $rating_review = $this->model_course->get_rating_courses($all_course['id']);
                if ($all_course['discount_price'] != 0) {
                    $get_discount_percent = ($all_course['discount_price'] / $all_course['price']) * 100;
                } elseif ($all_course['discount_price'] == 0) {
                    $get_discount_percent = 0;
                }
                $discount = intval($get_discount_percent);
                $data_array[$key] = [
                    "id_course" => $all_course['id'],
                    "instructor_id" => $all_course["instructor_id"],
                    "title" =>  $all_course['title'],
                    "price" => $all_course['price'],
                    "instructor_name" => $all_course['first_name'] . ' ' . $all_course['last_name'],
                    "discount_flag" => $all_course['discount_flag'],
                    "discount_price" => $all_course['discount_price'],
                    "thumbnail" => $this->model_course->get_thumbnail($all_course['id']),
                    "students" => $total_students,
                    "rating" => $rating_review,
                    "total_discount" => $discount,
                    "foto_profile" => $this->model_users->get_foto_profile($all_course['instructor_id']),
                    "top_course" => $all_course['is_top_course']

                ];
            }
            return $this->respond(get_response($data_array));
        } else {
            $course = $this->model_course->home_page_course();
            foreach ($course as $key => $all_course) {
                $total_students = $this->model_enrol->get_count_enrols_courses($all_course['id']);
                $rating_review = $this->model_course->get_rating_courses($all_course['id']);
                if ($all_course['discount_price'] != 0) {
                    $get_discount_percent = ($all_course['discount_price'] / $all_course['price']) * 100;
                } elseif ($all_course['discount_price'] == 0) {
                    $get_discount_percent = 0;
                }
                $discount = intval($get_discount_percent);
                $data[$key] = [
                    "id_course" => $all_course['id'],
                    "instructor_id" => $all_course["instructor_id"],
                    "title" =>  $all_course['title'],
                    "price" => $all_course['price'],
                    "instructor_name" => $all_course['first_name'] . ' ' . $all_course['last_name'],
                    "discount_flag" => $all_course['discount_flag'],
                    "discount_price" => $all_course['discount_price'],
                    "thumbnail" => $this->model_course->get_thumbnail($all_course['id']),
                    "students" => $total_students,
                    "rating" => $rating_review,
                    "total_discount" => $discount,
                    "foto_profile" => $this->model_users->get_foto_profile($all_course['instructor_id']),
                    "top_course" => $all_course['is_top_course']

                ];
            }
        }
        return $this->respond(get_response($data));
    }

    public function search_keyword_course($keyword = null)
    {
        if ($keyword) {
            $course = $this->model_course->search_course($keyword);
            if (is_null($course)) {
                return $this->failNotFound();
            } else {
                $course_search_data = $this->course_data($course);
                return $this->respond(get_response($course_search_data));
            }
        } else {
            return $this->respond([]);
        }
    }
    public function get_all_category()
    {
        $list_category = $this->model_category->list_category_home();
        return $this->respond(get_response($list_category));
    }

    public function wishlist()
    {
        $id_user = $this->request->getVar('users');
        if (!is_null($id_user)) {
            $item_wishlist = $this->model_wishlist->get_user_wishlist($id_user);
            if (!empty($item_wishlist)) {
                foreach ($item_wishlist as $wishlist) {
                    $total_students = $this->model_enrol->get_count_enrols_courses($wishlist->course_id);
                    $rating_review = $this->model_course->get_rating_courses($wishlist->course_id);
                    if ($wishlist->discount_price != 0) {
                        $get_discount_percent = ($wishlist->discount_price / $wishlist->price) * 100;
                    } elseif ($wishlist->discount_price == 0) {
                        $get_discount_percent = 0;
                    }
                    $data[] = [
                        "wishlist_id" => $wishlist->wishlist_id,
                        "course_id" => $wishlist->course_id,
                        "title" => $wishlist->title,
                        "price" => $wishlist->price,
                        "instructor" => $wishlist->first_name . ' ' . $wishlist->last_name,
                        "thumbnail" => $this->model_course->get_thumbnail($wishlist->course_id),
                        "discount_price" => $wishlist->discount_price,
                        "discount_flag" => $wishlist->discount_flag,
                        "total_discount" => intval($get_discount_percent),
                        "student" => $total_students,
                        "review" => $rating_review,
                        "foto_profile" => $this->model_users->get_foto_profile($wishlist->instructor_id),
                    ];
                }
                return $this->respond(get_response($data));
            } else {
                return $this->respond([
                    'status' => 200,
                    'error' => false,
                    'data' => []
                ]);
            }
        }
    }
    public function add_to_wishlist()
    {
        try {
            $wishlist_item = $this->request->getJSON();
            $check_wishlist = $this->model_wishlist->where(['id_user' => $wishlist_item->id_user, 'wishlist_item' => $wishlist_item->wishlist_item])->first();
            if (!empty($check_wishlist)) {
                $this->model_wishlist->delete($check_wishlist['id']);
                return $this->respondDeleted([
                    'status' => 200,
                    'error' => false,
                    'data' => [
                        'messages' => 'wishlist deleted !'
                    ]
                ]);
            } else {
                $this->model_wishlist->insert($wishlist_item);
                return $this->respondCreated([
                    'status' => 201,
                    'error' => false,
                    'data' => [
                        'messages' => 'Wishlist Success Added !'
                    ]
                ]);
            }
        } catch (\Throwable $th) {
            return $this->failNotFound();
        }
    }

    public function delete_wishlist($id_chart)
    {
        try {
            $this->model_wishlist->delete($id_chart);
            return $this->respondDeleted([
                'status' => 200,
                'error' => false,
                'data' => [
                    'messages' => 'wishlist deleted !'
                ]
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function delete_cart($id_cart)
    {
        $cart_data = $this->model_cart->find($id_cart);
        if (!is_null($cart_data)) {
            $this->model_cart->delete($id_cart);
            return $this->respondDeleted([

                'status' => 200,
                'error' => false,
                'data' => [
                    'messages' => 'cart deleted !'
                ]
            ]);
        }
    }

    public function cart_list($id_user)
    {
        $data_cart = array();
        $cart_items = $this->model_cart->cart_item_list($id_user);
        $total_payment = $this->model_cart->get_total_payment_cart($id_user);
        foreach ($cart_items as $key => $ci) {
            $total_students = $this->model_enrol->get_count_enrols_courses($ci->course_id);
            $rating_review = $this->model_course->get_rating_courses($ci->course_id);
            if ($ci->discount_price != 0) {
                $get_discount_percent = ($ci->discount_price / $ci->price) * 100;
            } elseif ($ci->discount_price == 0) {
                $get_discount_percent = 0;
            }
            $data_cart[$key] = [
                "cart_id" => $ci->cart_id,
                "course_id" => $ci->course_id,
                "title" => $ci->title,
                "price" => $ci->price,
                "instructor" => $ci->first_name . ' ' . $ci->last_name,
                "thumbnail" => $this->model_course->get_thumbnail($ci->course_id),
                "discount_price" => $ci->discount_price,
                "discount_flag" => $ci->discount_flag,
                "total_discount" => intval($get_discount_percent),
                "student" => $total_students,
                "review" => $rating_review,
                "foto_profile" => $this->model_users->get_foto_profile($ci->instructor_id),

            ];
        }
        return $this->respond([
            'status' => 200,
            'error' => false,
            'data' => $data_cart,
            'total_payment' => !empty($total_payment->cart_price) ? $total_payment->cart_price : null
        ]);
    }

    public function add_to_cart()
    {
        $data_cart = $this->request->getJSON();
        $find_price_course = $this->model_course->get_prices_for_cart($data_cart->cart_item);
        $price = $find_price_course->discount_flag != 0 ? $find_price_course->discount_price : $find_price_course->price;
        if (!is_null($find_price_course)) {
            $check_cart = $this->model_cart->where(['id_user' => $data_cart->id_user, 'cart_item' => $data_cart->cart_item])->first();
            if ($check_cart) {
                $this->model_cart->delete($check_cart['id']);
                return $this->respondDeleted([
                    'status' => 200,
                    'error' => false,
                    'data' => [
                        'messages' => 'cart delete !'
                    ]
                ]);
            }
            $this->model_cart->insert([
                'id_user' => $data_cart->id_user,
                'cart_item' => $data_cart->cart_item,
                'cart_price' => $price
            ]);
            return $this->respondCreated([
                'status' => 201,
                'error' => false,
                'data' => [
                    'messages' => 'cart added !'
                ]
            ]);
        } else {
            return $this->fail('data cannot to insert !');
        }
    }

    public function users_detail($id_user)
    {
        $user_detail = $this->model_users->get_detail_users($id_user);
        $social_user = $this->model_users_social_link->get_social_link($id_user);
        $data = [
            'id_user' => $user_detail->id,
            'fullname' => $user_detail->first_name . ' ' . $user_detail->last_name,
            'biography' => $user_detail->biography,
            'datebirth' => $user_detail->datebirth,
            'phone' => $user_detail->phone,
            "heading" => $user_detail->heading,
            "jenis_kelamin" => $user_detail->jenis_kel,
            'social_link' => !empty($social_user) ? $social_user : null,

        ];
        return $this->respond(get_response($data));
    }

    public function get_duration($lesson_duration)
    {
        $total_duration = 0;
        foreach ($lesson_duration->getResult('array') as $ld) {
            if ($ld['lesson_type'] != 'other') {
                $time = explode(':', $ld['duration']);
                $hour_to_seconds = $time[0] * 60 * 60;
                $minute_to_seconds = $time[1] * 60;
                $seconds = $time[2];
                $total_duration += $hour_to_seconds + $minute_to_seconds + $seconds;
            }
        }


        $hours = floor($total_duration / 3600);
        $minutes = floor(($total_duration % 3600) / 60);
        $seconds = $total_duration % 60;

        $h = $hours > 0 ? $hours . ($hours == 1 ? "j" : "j") : "";
        $m = $minutes > 0 ? $minutes . ($minutes == 1 ? "m" : "m") : "";
        $s = $seconds > 0 ? $seconds . ($seconds == 1 ? "d" : "d") : "";

        return $h . $m . $s;
    }

    public function filter()
    {
        $filter = array();
        if ($this->request->getVar('keyword')) {
            $keyword = $this->request->getVar('keyword');
            return $this->search_keyword_course($keyword);
        } else {
            if ($this->request->getVar()) {
                if (@$this->request->getVar('sub_category')) {
                    $category = str_replace(' ', ',', $this->request->getVar('sub_category'));
                    $filter[0]["a.sub_category_id"] = $category;
                }
                if ($this->request->getVar('price')) {
                    $price = $this->request->getVar('price');
                    $filter[0]["a.is_free_course"] = $price;
                }
                if ($this->request->getVar('level')) {
                    $level = $this->request->getVar('level');
                    $filter[0]["a.level_course"] = $level;
                }
                if ($this->request->getVar('language')) {
                    $language = $this->request->getVar('language');
                    $filter[0]["a.language"] = $language;
                }
                if ($this->request->getVar('rating')) {
                    $rating = $this->request->getVar('rating');
                    $filter[0]["d.rating"] = $rating;
                }
                if (empty($this->request->getVar('rating'))) {
                    $string_has_comma = @$filter[0]['a.sub_category_id'];
                    $sort = @$this->request->getVar('sort');
                    $sort_filter = $sort == 'max_price' ? ['params' => 'a.price', 'type' => 'DESC'] : ($sort == 'low_price' ? ['params' => 'a.price', 'type' => 'ASC']  : ($sort == 'review' ? ['params' => 'a.price', 'type' => 'DESC'] : null));
                    if (strpos($string_has_comma, ',') == true) {
                        $filter_category = $filter[0]['a.sub_category_id'];
                        $exp = explode(',', $filter_category);
                        $new_array_without_category = array_splice($filter[0], 1);
                        $data_filter = $this->model_course->advanced_filter($new_array_without_category, $exp, $sort_filter);
                    } else {
                        $data_filter = $this->model_course->advanced_filter($filter[0], $sort_filter);
                    }


                    if (is_null($data_filter)) {
                        return $this->failNotFound('not found!');
                    }
                    $data_response = $this->course_data($data_filter);
                    return $this->respond(get_response($data_response));
                } else {
                    $data_filter_rating = $this->model_course->get_rating_from_filter($filter[0]);
                    if (is_null($data_filter_rating)) {
                        return $this->failNotFound('not found!');
                    } else {
                        $data = [];
                        foreach ($data_filter_rating as $course) {
                            $data[] = [
                                "title" => $course->title,
                                "short_description" => $course->short_description,
                                "price" => $course->price,
                                "instructor_name" => $course->first_name . ' ' . $course->last_name,
                                "discount_flag" => $course->discount_flag,
                                "discount_price" => $course->discount_price,
                                "thumbnail" => $course->thumbnail,
                                "level_course" => $course->level_course,
                                "total_lesson" => $course->total_lesson,
                                "id" => $course->id,
                                "instructor_id" => $course->instructor_id,
                                "language" => $course->language
                            ];
                        }
                        return $this->respond(get_response($data));
                    }
                }
            } else {
                return $this->failNotFound('not found !');
            }
        }
    }

    public function course_data($course_data)
    {
        $data = array();
        foreach ($course_data as $key => $cd) {
            $lesson = $this->model_course->get_lesson_duration(['course_id' => $cd['id']]);
            $total_students = $this->model_enrol->get_count_enrols_courses($cd['id']);
            $rating_review = $this->model_course->get_rating_courses($cd['id']);
            $duration = $this->get_duration($lesson);
            if ($cd['discount_price'] != 0) {
                $get_discount_percent = ($cd['discount_price'] / $cd['price']) * 100;
            } elseif ($cd['discount_price'] == 0) {
                $get_discount_percent = 0;
            }
            $discount = intval($get_discount_percent);
            $data[$key] = [
                "id_course" => $cd['id'],
                "instructor_id" => $cd["instructor_id"],
                "title" =>  $cd['title'],
                "short_description" => $cd['short_description'],
                "price" => $cd['price'],
                "instructor_name" => $cd['first_name'] . ' ' . $cd['last_name'],
                "discount_flag" => $cd['discount_flag'],
                "discount_price" => $cd['discount_price'],
                "thumbnail" => $this->model_course->get_thumbnail($cd['id']),
                "level_course" => $cd['level_course'],
                "total_lesson" => $cd['total_lesson'],
                "language" => $cd['language'],
                "duration" => $duration,
                "students" => $total_students,
                "foto_profile" => $this->model_users->get_foto_profile($cd['id']),
                "rating" => $rating_review,
                "total_discount" => $discount

            ];
        }
        return $data;
    }
    public function detail_courses($id_course)
    {
        $data = array();
        $data_course = $this->model_course->detail_course_for_homepage($id_course);
        $is_mine = null;
        if ($this->request->getVar('id_user')) {
            $token = $this->request->getServer('HTTP_AUTHORIZATION');
            if (is_null($token)) {
                return $this->failUnauthorized();
            } else {
                $user_id = $this->request->getVar('id_user');
                if (verify_request($token, $user_id)) {
                    $user_id = $this->request->getVar('id_user');
                    $check_enrolled = $this->model_enrol->where(['user_id' => $user_id, 'course_id' => $id_course])->get()->getResult();
                    if ($check_enrolled) {
                        $is_mine = 1;
                    } else {
                        $is_mine = 0;
                    }
                } else {
                    return $this->failUnauthorized();
                }
            }
        }
        foreach ($data_course as $key => $courses) {
            $get_breadcrumbs = $this->model_category->getCategory($courses->category_id);
            $get_breadcrumbs_sub_category = $this->model_category->getCategory($courses->sub_category_id);
            $total_duration_by_course_id = $this->model_course->get_lesson_duration(['course_id' => $courses->id]);
            $total_duration = $this->get_duration($total_duration_by_course_id);
            $total_students = $this->model_enrol->get_count_enrols_courses($courses->id);
            if ($courses->discount_price != 0) {
                $get_discount_percent = ($courses->discount_price / $courses->price) * 100;
            } elseif ($courses->discount_price == 0) {
                $get_discount_percent = 0;
            }
            $discount = intval($get_discount_percent);
            $data[$key] = [
                'id' => $courses->id,
                'title' => $courses->title,
                'instructor_id' => $courses->uid,
                'instructor' => $courses->first_name . ' ' . $courses->last_name,
                'short_description' => $courses->short_description,
                'level_course' => $courses->level_course,
                'total_lesson' => $courses->total_lesson,
                'total_students' => $total_students,
                'description' => $courses->description,
                'outcome' => $courses->outcomes,
                'requirement' => $courses->requirement,
                'price' => $courses->price,
                'discount_price' => $courses->discount_price,
                'discount_flag' => $courses->discount_flag,
                'video_url' => $courses->video_url,
                'total_duration' => $total_duration,
                'bio' => $this->model_course->get_bio_instructor(['id_user' => $courses->uid, 'bio_status' => $courses->bio_status, 'bio_instructor' => $courses->bio_instructor]),
                'rating' => $this->model_course->get_rating_courses($courses->id),
                'total_discount' => intval($get_discount_percent),
                'last_modified' => !is_null($courses->update_at) ? generate_humanize_timestamps($courses->update_at) : generate_humanize_timestamps($courses->create_at),
                "foto_profile" => $this->model_users->get_foto_profile($courses->uid),
                "thumbnail" => $this->model_course->get_thumbnail($courses->id),
                "is_free_course" => $courses->is_free_course,
                'is_mine' => $is_mine,
                'breadcrumbs' => [
                    'id_category' => @$get_breadcrumbs->id,
                    'parent_name' => @$get_breadcrumbs->name_category,
                    'sub_category_id' => @$get_breadcrumbs_sub_category->id,
                    'sub_parent_category' => @$get_breadcrumbs_sub_category->name_category
                ]

            ];
        }
        return $this->respond(get_response($data));
    }

    public function get_instructor_student($instructor_id)
    {
        $data = $this->model_course->detail_intructor_by_courses($instructor_id);

        return $this->respond(get_response($data));
    }

    public function get_sections_duration($course_id)
    {
        $arr_data = array();
        $data = $this->model_course->get_section_duration($course_id);
        foreach ($data as $key => $section) {
            $string_to_array_section = preg_split('/[[,\]]/', $section);
            for ($i = 0; $i < count($string_to_array_section); $i++) {
                if ($string_to_array_section[$i] == null) {
                    continue;
                }
                $duration_by_section = $this->model_course->get_lesson_duration(['section' => $string_to_array_section[$i]]);
                $total_duration_section = $this->get_duration($duration_by_section);
                $arr_data[] = $total_duration_section;
            }
        }
        return $arr_data;
    }

    public function get_sections($id_course)
    {
        $data = array();
        $total_duration = $this->get_sections_duration($id_course);
        $section_title = $this->model_course->get_section_title($id_course);
        foreach ($section_title as $key => $st) {
            $course_by_section  = $this->model_course->lesson_title_from_section($st->id);
            $data[$key] = [
                'title' => $st->title,
                'duration' => !empty($total_duration[$key]) ? $total_duration[$key] : null,
                'data_lesson' => [
                    $course_by_section
                ]
            ];
        }
        return $this->respond(get_response($data));
    }
    public function get_rating($course_id)
    {
        if ($this->request->getVar('star')) {
            $star = $this->request->getVar('star');
            $data_rating = $this->model_course->get_rating_by_star($course_id, $star);
            $data = [];
            foreach ($data_rating as $r) {
                $data[] = [
                    'user' => $r->first_name . ' ' . $r->last_name,
                    'review' => $r->review,
                    'rating' => $r->rating,
                    'date' => generate_humanize_timestamps($r->create_at)
                ];
            }
            return $this->respond([
                'status' => 200,
                'error' => false,
                'data' => $data
            ]);
        }
        $data_rating = $this->model_course->get_rating_course($course_id);
        return $this->respond($data_rating);
    }
    public function user_profile($id = null)
    {
        $user_id = $this->model_users->find($id);

        $rules = [
            'first_name' => [
                'rules' => 'required'
            ],
            'biography' => [
                'rules' => 'required'
            ],
            'phone' => [
                'rules' => 'required'
            ],
            'phone' => [
                'rules' => 'required'
            ],
            'datebirth' => [
                'rules' => 'required'
            ],
            'heading' => [
                'rules' => 'required'
            ],
            'jenis_kel' => [
                'rules' => 'required'
            ],
        ];

        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => false,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {
            if (!empty($user_id)) {

                $update = $this->request->getJSON();
                // var_dump($update);
                // die;
                $this->model_users->update($id, $update);

                $user_detail = $this->model_users_detail->where('id_user', $id)->first();

                if ($user_detail) {
                    $user['id_user'] = $id;
                    $user['biography'] = $update->biography;
                    $user['phone'] = $update->phone;
                    $user['datebirth'] = $update->datebirth;
                    $user['heading'] = $update->heading;
                    $user['jenis_kel'] = $update->jenis_kel;
                    $this->model_users_detail->where('id_user', $id)->set($user)->update();

                    if (!empty($update->social_link)) {
                        $user_social_link = $this->model_users_social_link->where('id_user', $id)->first();

                        if ($user_social_link) {
                            $user['id_user'] = $id;
                            $user['facebook'] = !empty($update->social_link->facebook) ? $update->social_link->facebook : $user_social_link['facebook'];
                            $user['instagram'] = !empty($update->social_link->instagram) ? $update->social_link->instagram : $user_social_link['instagram'];
                            $user['twitter'] = !empty($update->social_link->twitter) ? $update->social_link->twitter : $user_social_link['twitter'];
                            $user['linkedin'] = !empty($update->social_link->linkedin) ? $update->social_link->linkedin : $user_social_link['linkedin'];
                            $user['youtube'] = !empty($update->social_link->youtube) ? $update->social_link->youtube : $user_social_link['youtube'];
                            $this->model_users_social_link->where('id_user', $id)->set($user)->update();
                        } else {
                            $user['id_user'] = $id;
                            $user['facebook'] = !empty($update->social_link->facebook) ? $update->social_link->facebook : null;
                            $user['instagram'] = !empty($update->social_link->instagram) ? $update->social_link->instagram : null;
                            $user['twitter'] = !empty($update->social_link->twitter) ? $update->social_link->twitter : null;
                            $user['linkedin'] = !empty($update->social_link->linkedin) ? $update->social_link->linkedin : null;
                            $user['youtube'] = !empty($update->social_link->youtube) ? $update->social_link->youtube : null;
                            $this->model_users_social_link->save($user);
                        }
                    }
                } else {
                    $user['id_user'] = $id;
                    $user['biography'] = $update->biography;
                    $user['phone'] = $update->phone;
                    $this->model_users_detail->save($user);
                }

                return $this->respondUpdated(response_update());
            } else {
                return $this->failNotFound();
            }
        }
    }
    public function user_credentials($id = null)
    {
        $user = $this->model_users->find($id);

        $rules = [
            'email' => [
                'rules' => 'required|valid_email'
            ],
            'old_password' => [
                'rules' => 'required|min_length[6]'
            ],
            'password' => [
                'rules' => 'required|min_length[6]'
            ],
            'new_password_confirm' => [
                'rules' => 'required|matches[password]'
            ]
        ];

        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => false,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {
            $update = $this->request->getJSON();
            $data['email'] = $update->email;
            $data['old_password'] = sha1($update->old_password);
            $data['password'] = sha1($update->password);
            $data['new_password_confirm'] = sha1($update->new_password_confirm);

            if ($data['old_password'] !== $user['password']) {
                return $this->respond([
                    'status' => 403,
                    'error' => false,
                    'data' => [
                        'message' => 'Wrong current password'
                    ]
                ], 403);
            } else {
                if ($data['new_password_confirm'] === $user['password']) {
                    return $this->respond([
                        'status' => 403,
                        'error' => false,
                        'data' => [
                            'message' => 'New Password cannot be the same as current password'
                        ]
                    ], 403);
                } else {
                    $this->model_users->update($id, $data);
                    return $this->respondUpdated(response_update());
                }
            }
        }
    }
    public function user_photo($id = null)
    {
        $rules = [
            'foto_profile' => [
                'rules' => 'uploaded[foto_profile]|is_image[foto_profile]',
                'errors' => [
                    'uploaded' => 'foto_profile must be uploaded',
                    'is_image' => 'what you choose is not a picture',
                    'mime_in' => 'what you choose is not a picture'
                ]
            ]
        ];

        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => false,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {
            $user = $this->model_users->find($id);
            $id_user = $user['id'];
            if ($user) {
                $foto_profile = $this->request->getFile('foto_profile');
                $name = "foto_profile_default_$id_user.jpg";
                $folder_path = 'uploads/foto_profile/' . $name;

                $data = [
                    'id' => $id,
                    'foto_profile'  => $name
                ];

                if (file_exists('uploads/foto_profile')) {
                    if (file_exists($folder_path)) {
                        unlink('uploads/foto_profile/' . $name);
                    }
                    $foto_profile->move('uploads/foto_profile/', $name);
                    // $this->model_users_detail->update($id, $data);
                    return $this->respondCreated(response_create());
                } else {
                    mkdir('uploads/foto_profile');
                    if (file_exists($name)) {
                        unlink('uploads/foto_profile/' . $name);
                    }
                    $foto_profile->move('uploads/foto_profile/', $name);
                    // $this->model_users_detail->update($id, $data);

                    return $this->respondCreated(response_create());
                }
            } else {
                return $this->failNotFound();
            }
        }
    }

    public function redeem_voucher()
    {
        $voucher = $this->request->getVar('voucher');
        $course_id = $this->request->getVar('course_id');
        $id_user = $this->request->getVar('user_id');
        $verify_voucher = $this->model_course->verify_redeem_voucher($voucher, $course_id);
        if (!is_null($verify_voucher)) {
            $this->model_cart->insert([
                'id_user' => $id_user,
                'cart_item' => $course_id,
                'cart_price' => $verify_voucher
            ]);
            return $this->respondCreated([
                'status' => 201,
                'error' => false,
                'data' => [
                    'messages' => 'voucher redeem !'
                ]
            ]);
        } else {
            return $this->failNotFound('Voucher invalid !');
        }
    }

    public function my_course($user_id)
    {
        $data = array();
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        if (verify_request($token, $user_id) == true) {
            $my_course = $this->model_course->my_course($user_id);
            if (!empty($my_course)) {
                foreach ($my_course as $key => $values) {
                    $total_lesson = $this->model_course->get_count_lesson_course($values->cid);
                    $progress = $this->model_course->get_count_progress($values->cid, $user_id);
                    $total_progress = $progress == 0 ? 0 : ($progress / $total_lesson->total_lesson) * 1;
                    $progress_course = intval($total_progress * 100) >= 100 ? 100 : intval($total_progress * 100);
                    $get_type_video = $this->model_lesson->get_type_video_lesson($values->cid);
                    $data[$key] = [
                        'course_id' => $values->cid,
                        'instructor_id' => $values->instructor_id,
                        'instructor' => $values->first_name . ' ' . $values->last_name,
                        'title' => $values->title,
                        'thumbnail' => $this->model_course->get_thumbnail($values->cid),
                        'rating' => $this->model_course->rating_from_user($user_id, $values->cid),
                        "foto_profile" => $this->model_users->get_foto_profile($values->instructor_id),
                        "total_progress" => $progress == 0 ? 0 : $progress_course,
                        "video_type" => strtolower($get_type_video)
                    ];
                }
                return $this->respond(get_response($data));
            } else {
                return $this->failNotFound();
            }
        } else {
            return $this->fail('invalid request !');
        }
    }

    public function my_lesson()
    {
        $course_id = $this->request->getVar('course');
        $user_id = $this->request->getVar('user');
        $where = [
            'course_id' => $course_id,
            'user_id' => $user_id
        ];
        $check_user = $this->model_enrol->where($where)->first();
        if (!is_null($check_user)) {
            $data_lesson = $this->model_course->get_my_lesson($course_id);
            $data = array();
            $progress_data = array();
            foreach ($data_lesson as $key => $dl) {
                $total_lesson = $this->model_course->get_count_lesson_course($dl->course_id);
                $progress = $this->model_course->get_count_progress($dl->course_id, $user_id);
                $total_progress = $progress == 0 ? 0 : ($progress / $total_lesson->total_lesson) * 1;
                $progress_course = intval($total_progress * 100) >= 100 ? 100 : intval($total_progress * 100);
                $progress_data[0] = $progress == 0 ? 0 : $progress_course;
                $data[$key] = [
                    'course_id' => $dl->course_id,
                    'lesson_id' => $dl->id,
                    'section_title' => $dl->section_title,
                    'lesson_title' => $dl->lesson_title,
                    'duration' => $dl->duration,
                    'attachment_type' => $dl->attachment_type,
                    'video_type' => $dl->video_type !== "" ? $dl->video_type : null,
                    'video_url' => $dl->video_url !== "" ? $dl->video_url : null,
                    'lesson_type' => $dl->lesson_type,
                    'attachment' => $dl->attachment !== "" ? $this->model_course->check_lesson_file($dl->attachment) : null,
                    'is_skip' => $dl->is_skip,
                    'is_finished' => $this->model_watch->check_finished($user_id, $dl->id, $dl->course_id)


                ];
            }
            return $this->respond([
                'status' => 200,
                'error' => false,
                'progress' => @$progress_data[0],
                "data" => [$data]
            ]);
        } else {
            return $this->failForbidden('Cannot Access This Course');
        }
    }

    public function watch_history()
    {
        $watch_history = $this->request->getJSON();
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        if (verify_request($token, $watch_history->id_user) == true) {
            $check_enrolled = $this->model_enrol->where(['user_id' => $watch_history->id_user, 'course_id' => $watch_history->course_id])->first();
            if (!empty($check_enrolled)) {
                if (empty($this->model_watch->where(['id_user' => $watch_history->id_user, 'course_id' => $watch_history->course_id, 'lesson_id' => $watch_history->lesson_id])->first())) {
                    $update_progress = $this->model_watch->insert($watch_history);
                    return $this->respondCreated(response_create());
                } else {
                    return $this->failForbidden('you cannot access this resources !');
                }
            } else {
                return $this->failNotFound('data not found !');
            }
        } else {
            return $this->fail('invalid request !');
        }
    }

    public function get_watch_history($user_id)
    {
        $watch_history = $this->model_watch->where('id_user', $user_id)->get()->getResult();
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        if (verify_request($token, $user_id) == true) {
            return $this->respond(get_response($watch_history));
        } else {
            return $this->fail('invalid request !');
        }
    }

    public function review($id)
    {
        if ($this->request->getVar('update')) {
        }

        $rules = $this->model_review->validationRules;
        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => true,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {

            $value =  $this->request->getJSON();
            $data = [
                'review' => $value->review,
                'ratable_id' => $value->course_id,
                'ratable_type' => 'course',
                'rating' => $value->rating,
                'create_at' => strtotime(date('D, d-M-Y')),
                'user_id' => $id,
            ];

            $this->model_review->review($data);
            return $this->respondCreated(response_create());
        }
    }

    public function sertifikat($user_id)
    {
        $course = $this->request->getVar('course_id');
        if (!$course) {
            return $this->failNotFound();
        }

        $user = $this->model_enrol->get_sertifikat($user_id, $course);
        $section = $this->model_enrol->count_section($course);
        $watch_progress = $this->model_watch->count_progress($course, $user_id);
        $progress = ($watch_progress / $section) * 1;

        if ($user && $course && $watch_progress) {
            if ($progress === 1) {
                $update = [
                    'finish_date' => Time::now()
                ];

                $this->model_enrol->protect(false)->update($user->id, $update);
            }
            $enrol = $this->model_enrol->get_sertifikat($user_id, $course);
            $data = [
                'name' => $enrol->first_name . ' ' . $enrol->last_name,
                'title' => $enrol->title,
                'finish_date' => strtotime($enrol->finish_date),
                'certificate_no' => $this->model_payment->get_payment_user($user_id, $enrol->course_id)
            ];
            return $this->respond(get_response($data));
        } else {
            return $this->failNotFound();
        }
    }

    public function qna_thread()
    {
        $id_user = $this->request->getVar('id_user');
        $course_id = $this->request->getVar('course_id');
        $get_qna = $this->model_qna->get_qna($course_id);
        $data_array = array();
        foreach ($get_qna as $dq) {
            $data_array[] = [
                'id_qna' => $dq->id_qna,
                'sender' => $dq->sender,
                'username' => $dq->first_name . ' ' . $dq->last_name,
                'quest' => $dq->quest,
                'foto_profile' => $this->model_users->get_foto_profile($dq->sender),
                'like' => $dq->up,
                'comment' => $this->model_qna->get_comment($dq->id_qna),
            ];
        }

        return $this->respond(get_response($data_array));
    }

    public function create_qna()
    {
        $rules = [
            'quest' => 'required'
        ];
        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            $data_qna = $this->request->getJSON();
            $this->model_qna->insert($data_qna);
            return $this->respondCreated(response_create());
        }
    }

    public function qna_reply()
    {
        $rules = [
            'text_rep' => 'required',
        ];

        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            $request = $this->request->getJSON();
            $this->model_qna_reply->insert($request);
            return $this->respondCreated(response_create());
        }
    }



    public function payment_free_course()
    {
        $request_data = $this->request->getJSON();
        $user_id = $request_data->user_id;
        $course_id = $request_data->course_id;
        $payment_id = $user_id . rand();
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        $check_free_course = $this->model_course->find($course_id);
        if (verify_request($token, $user_id) == true) {
            if (!empty($this->model_enrol->where(['user_id' => $user_id, 'course_id' => $course_id])->get()->getResult())) {
                return $this->fail('you have enrolled this course !');
            } else {
                if ($check_free_course['is_free_course'] == 1) {
                    $redeem_course = $this->model_payment->insert([
                        'id_payment' => $payment_id,
                        'id_user' => $user_id,
                        'course_id' => $course_id,
                        'payment_type' => 'free',
                        'amount' => 0,
                        'admin_revenue' => 0,
                        'instructor_revenue' => 0,
                        'status_payment' => 0,
                        'status' => 1
                    ]);
                } else {
                    return $this->failForbidden('this course is not free !');
                }
                if (empty($this->model_enrol->where('payment_id', $payment_id)->get()->getResult())) {
                    $enrol_course = $this->model_enrol->insert([
                        'user_id' => $user_id,
                        'course_id' => $course_id,
                        'payment_id' => $payment_id
                    ]);
                }


                return $this->respondCreated([
                    'status' => 201,
                    'error' => false,
                    'messages' => 'Course Enrolled'
                ]);
            }
        } else {
            return $this->fail('you cannot access this resources !');
        }
    }

    public function history_payment($id_user)
    {
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        if (verify_request($token, $id_user) == true) {
            $payment_history = $this->model_payment->history_payment_user($id_user);
            $data_payment = array();
            if ($payment_history) {
                foreach ($payment_history as $ph) {
                    $data_payment[] = [
                        "code_payment" => $ph->id_payment,
                        'title' => $ph->title,
                        'thumbnail' => $this->model_course->get_thumbnail($ph->cid),
                        'instructor' => $ph->first_name . ' ' . $ph->last_name,
                        'foto_profile' => $this->model_users->get_foto_profile($ph->uid),
                        'total_price' => $ph->discount_flag == 0 ? $ph->price : $ph->discount_price,
                        'date' => generate_humanize_timestamps($ph->create_at),
                        'payment_type' => $ph->payment_type,
                        'status_payment' => $ph->status_payment
                    ];
                }
                return $this->respond(get_response($data_payment));
            } else {
                return $this->respond(get_response($data_payment));
            }
        } else {
            return $this->fail('invalid_request');
        }
    }

    public function last_progress_lesson($user_id)
    {
        $data_array = array();
        $get_last_watch_history = $this->model_watch->last_progress($user_id);
        foreach ($get_last_watch_history as $key => $wh) {
            $data_array[$key] = [
                'course_id' => $wh->course_id,
                'lesson_id' => $wh->lesson_id,
                'section_title' => $wh->lesson_title,
                'course_title' => $wh->course_title,
                'video_url' => $wh->video_url,
                'thubmnail' => $this->model_course->get_thumbnail($wh->course_id)

            ];
        }

        return $this->respond(get_response($data_array));
    }

    public function pagging_course($page, $offset)
    {
        $start_index = ($page > 1) ? ($page * $offset) - $offset : 0;
        $count_data = $this->model_course->get_count_course();
        $total_pages = ceil($count_data / $offset);
        $pagging_home = $this->model_course->pagging_home($offset, $start_index);
        $data = array();
        foreach ($pagging_home as $key =>  $all_course) {
            $total_students = $this->model_enrol->get_count_enrols_courses($all_course['id']);
            $rating_review = $this->model_course->get_rating_courses($all_course['id']);
            if ($all_course['discount_price'] != 0) {
                $get_discount_percent = ($all_course['discount_price'] / $all_course['price']) * 100;
            } elseif ($all_course['discount_price'] == 0) {
                $get_discount_percent = 0;
            }
            $discount = intval($get_discount_percent);
            $data[$key] = [
                "id_course" => $all_course['id'],
                "instructor_id" => $all_course["instructor_id"],
                "title" =>  $all_course['title'],
                "price" => $all_course['price'],
                "instructor_name" => $all_course['first_name'] . ' ' . $all_course['last_name'],
                "discount_flag" => $all_course['discount_flag'],
                "discount_price" => $all_course['discount_price'],
                "thumbnail" => $this->model_course->get_thumbnail($all_course['id']),
                "students" => $total_students,
                "rating" => $rating_review,
                "total_discount" => $discount,
                "foto_profile" => $this->model_users->get_foto_profile($all_course['instructor_id']),
                "top_course" => $all_course['is_top_course']

            ];
        }

        $data_course = [
            'total_pages' => $total_pages,
            'data_course' => $data
        ];
        return $data_course;
    }

    public function announcement_lesson($id_course)
    {
        if ($id_course) {
            $announcement = $this->model_course->get_announcement($id_course);
            $data_array = array();
            foreach ($announcement as $key => $ann) {
                $data_array[$key] = [
                    'id_announcement' => $ann->id_ann,
                    'token_announcement' => $ann->token,
                    'instructor_name' => $ann->first_name . ' ' . $ann->last_name,
                    'foto_profile' => $this->model_users->get_foto_profile($ann->id_instructor),
                    'body_content' => $ann->body,
                    'likes' => $ann->user_likes,
                    'comment' => $this->model_course->count_comment($ann->token),
                    "date" => generate_humanize_timestamps($ann->create_at),
                    'replies' => $this->model_course->announcement_replies($ann->token, $ann->course_id),
                ];
            }
            return $this->respond(get_response($data_array));
        }
    }

    public function create_announcement()
    {
        $rules = $this->model_announcement->validationRules;
        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            try {
                $request = $this->request->getJSON();
                $token_announcement = generateRandomString(10);
                $this->model_announcement->insertData([
                    'token' => $token_announcement,
                    'sender' => $request->id_user,
                    'body' => $request->body,
                    'course_id' => $request->course_id,
                    'create_at' => strtotime(date('y-m-d'))
                ]);
                return $this->respondCreated(response_create());
            } catch (\Exception $e) {
                return $this->respond($e->getMessage());
            }
        }
    }

    public function reply_announcement()
    {
        $rules = $this->model_replies_ann->validationRules;

        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            try {
                $request = $this->request->getJSON();
                $this->model_replies_ann->insert([
                    'token' => $request->token,
                    'sender' => $request->sender,
                    'replies' => $request->replies,
                    'body' => $request->body
                ]);
                return $this->respondCreated(response_create());
            } catch (\Exception $e) {
                return $this->fail($e->getMessage());
            }
        }
    }

    public function likes_announcement()
    {
        $rules = [
            'id_user' => 'required',
            'token' => 'required'
        ];

        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            try {
                $request = $this->request->getJSON();
                $find_announcement = $this->model_announcement->where('token', $request->token)->first();
                $count_likes = $find_announcement['user_likes'];
                $data_likes = $count_likes + 1;
                $recent_likes = $this->model_course->user_like($data_likes, $request->token);
                return $this->respond([
                    'user_likes' => $recent_likes
                ]);
            } catch (\Exception $e) {
                return $this->fail($e->getMessage());
            }
        }
    }

    public function press_kit_dark()
    {
        $folder = "uploads/press_kit/logo-dark.png";

        $data = [
            'logo_dark' => base_url() . '/' . $folder
        ];

        return $this->respond(get_response($data));
    }

    public function press_kit_light()
    {
        $folder = "uploads/press_kit/logo-light.png";

        $data = [
            'logo_light' => base_url() . '/' . $folder
        ];

        return $this->respond(get_response($data));
    }
    
    public function exchange_voucher(){
        
        $voucher = $this->request->getVar('voucher');
        $additation = $this->request->getVar('additation');

        if ($voucher) {
            $data_voucher = $this->model_coupon->redeem($voucher);
            $decode_voucher = json_decode($data_voucher->course_id);
            
            if ($additation) {
                $data_additation = $this->model_coupon->redeem($additation);
                $decode_additation = json_decode($data_additation->course_id);
        
                    $data = array();
                    foreach ($decode_voucher as $key => $v) {
                        $data[$key] = [
                            "id_coupon" => $v,
                            "code_coupon" => $data_voucher->code_coupon,
                            "course" => $this->model_coupon->course($v),
                        ];
                    }
        
                    $tampil_additation = array();
                    foreach ($decode_additation as $key => $a) {
                        $tampil_additation[$key] = [
                            "id_coupon" => $a,
                            "code_coupon" => $data_additation->code_coupon,
                            "course" => $this->model_coupon->course($a),
                        ];
                    }
                    $respons = [
                        "voucher" => $data,
                        "additation" => $tampil_additation
                    ];
                    return $this->respond(get_response($respons));
                }
                else {
                    $data = array();
                    foreach ($decode_voucher as $key => $v) {
                        $data[$key] = [
                            "id_coupon" => $v,
                            "code_coupon" => $data_voucher->code_coupon,
                            "course" => $this->model_coupon->course($v),
                        ];
                    }
                    $respons = [
                        "voucher"=> $data
                    ];
                    return $this->respond(get_response($respons));
                    
                }
        }else {
            return $this->failNotFound();
        }

    }
}
