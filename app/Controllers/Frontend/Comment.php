<?php

namespace App\Controllers\Frontend;

use App\Controllers\Frontend\FrontendController;

class Comment extends FrontendController
{
    protected $format = 'json';

    public function __construct()
    {
        helper('parse_date');
    }

    public function show($id_course = null)
    {
        $comment = $this->model_comment->get_comment_by_course($id_course);
        if ($comment) {
            foreach ($comment as $c) {
                $data[] = [
                    "id_comment" => $c->id,
                    "commentable_type" => $c->commentable_type,
                    "body" => $c->body,
                    "user_id" => $c->user_id,
                    "user" => $c->first_name . ' ' . $c->last_name,
                    "foto_profil" => $this->model_users_detail->get_profile_users($c->user_id),
                    "date_added" => generate_humanize_timestamps($c->date_added),
                    "last_modified" => generate_humanize_timestamps($c->last_modified),
                ];
            }
            return $this->respond(get_response($data));
        } else {
            return $this->failNotFound();
        }
    }

    public function create_by_course()
    {
        $rules = $this->model_comment->validationRules;
        $data_comment = $this->request->getJSON();
        $user = $this->model_users->find($data_comment->user_id);
        $courses = $this->model_course->find($data_comment->course_id);
        if (!$this->validate($rules)) {
            return $this->fail("Failed To Create Please Try Again");
        } else {
            if ($user && $courses) {
                $data_comment = $this->request->getJSON();
                $this->model_comment->insert([
                    'body' => $data_comment->body,
                    'user_id' => $data_comment->user_id,
                    'commentable_id' => $data_comment->course_id,
                    'commentable_type' => 'course',
                ]);
                return $this->respondCreated(response_create());
            } else {
                return $this->failNotFound();
            }
        }
    }

    public function update($id = null)
    {
        $data_comment = $this->model_comment->find($id);

        if ($id) {
            $data = array(
                'body' => $this->request->getJsonVar('body')
            );
            $this->model_comment->update($id, $data);
            return $this->respond(response_update());
        } else {
            return $this->failNotFound();
        }
    }

    public function  delete($id = null)
    {
        $data_comment = $this->model_comment->find($id);

        if ($data_comment) {
            $this->model_comment->delete($id);
            return $this->respondDeleted(response_delete());
        } else {
            return $this->failNotFound();
        }
    }
}
