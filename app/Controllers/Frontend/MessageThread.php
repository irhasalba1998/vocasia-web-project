<?php

namespace App\Controllers\Frontend;

use App\Controllers\Frontend\FrontendController;
use CodeIgniter\I18n\Time;

class MessageThread extends FrontendController
{
    protected $format = 'json';

    public function __construct()
    {
        $this->validation = \Config\Services::validation();
    }
    public function create()
    {
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        $json_message = $this->request->getJSON();
        $rules = [
            'message_body' => 'required'
        ];

        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => false,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {
            if (verify_request($token, $json_message->sender_id)) {
                $code = str_split('abcdefghijklmnopqrstuvwxyz' . '0123456789');
                shuffle($code);
                $code_rand = '';
                $time = new Time();
                foreach (array_rand($code, 15) as $k) $code_rand .= $code[$k];
                $check_message_exist = $this->model_message_thread->check_message($json_message->sender_id, $json_message->receiver_id);
                if (!empty($check_message_exist)) {
                    return $this->fail('user id receiver already exist in listing chat');
                } else {
                    $data = [
                        'message_thread_code' => $code_rand,
                        'message' => $json_message->message_body,
                        'sender' =>  $json_message->sender_id,
                        'read_status' => 0,
                        "timestamp" => strtotime($time->now('Asia/Jakarta', 'en_US'))
                    ];

                    $data_message_thread = [
                        'message_thread_code' => $code_rand,
                        'sender' => $json_message->sender_id,
                        'receiver' => $json_message->receiver_id,
                        'last_message_timestamp' => strtotime($time->now('Asia/Jakarta', 'en_US'))
                    ];
                    $this->model_message->insert($data);
                    $this->model_message_thread->insert($data_message_thread);
                    return $this->respondCreated(response_create());
                }
            } else {
                return $this->fail('invalid user !');
            }
        }
    }

    public function listing_chat($id_user)
    {
        $token = $this->request->getServer('HTTP_AUTHORIZATION');
        if (verify_request($token, $id_user)) {
            $list_chat = $this->model_message_thread->where('sender', $id_user)->get()->getResult();
            $data = array();
            foreach ($list_chat as $lc) {
                $username = $this->model_message->get_receiver($lc->receiver);
                $last_message = $this->model_message->get_last_message($lc->message_thread_code, $lc->last_message_timestamp);
                $data[] = [
                    'message_thread_code' => $lc->message_thread_code,
                    'foto_profile' => $this->model_users->get_foto_profile($lc->receiver),
                    'username' => $username->first_name . ' ' . $username->last_name,
                    'preview_last_message' => @$last_message->message,
                    'date' => generate_humanize_timestamps($lc->last_message_timestamp)

                ];
            }
            return $this->respond(get_response($data));
        } else {
            return $this->fail('invalid user !');
        }
    }

    public function room_chat()
    {
        $message_thread_code = $this->request->getVar('code');
        $data = array();
        $get_message = @$this->model_message->where('message_thread_code', $message_thread_code)->get()->getResult();
        foreach ($get_message as $key => $gm) {
            $message_by_timestamps = @$this->model_message->where(['message_thread_code' => $message_thread_code, 'timestamp' => $gm->timestamp])->get()->getResult();
            foreach ($message_by_timestamps as $mt) {
                $data[] = [
                    "message_id" => $mt->message_id,
                    "sender" => $mt->sender,
                    "foto_profile" => $this->model_users->get_foto_profile($mt->sender),
                    "message" => $mt->message,
                    "timestamp" => $mt->timestamp
                ];
            }
        }

        return $this->respond(get_response($data));
    }

    public function reply_chat()
    {
        $time = new Time();
        $json_message = $this->request->getJSON();

        $rules = [
            'message_body' => 'required'
        ];

        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => false,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {
            $data = [
                'message_thread_code' => $json_message->message_thread_code,
                'message' => $json_message->message_body,
                'sender' =>  $json_message->sender_id,
                'read_status' => 0,
                'timestamp' => strtotime($time->now('Asia/Jakarta', 'en_US'))
            ];
            $this->model_message->insert($data);

            $data_thread = [
                'last_message_timestamp' => strtotime($time->now('Asia/Jakarta', 'en_US'))
            ];
            $this->model_message_thread->set($data_thread)->where('message_thread_code', $json_message->message_thread_code)->update();

            return $this->respond(response_create());
        }
    }
}
