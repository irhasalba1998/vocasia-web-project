<?php

namespace App\Controllers;

use App\Models\UsersModel;
use CodeIgniter\I18n\Time;
use CodeIgniter\RESTful\ResourceController;

class EmailController extends ResourceController
{
    public function __construct()
    {
        $this->user_model = new UsersModel();
        $this->validation = \Config\Services::validation();
        helper('response');
    }

    public function request()
    {
        $email = \Config\Services::email();
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $time = new Time();
        $time_now = strtotime($time->now('Asia/Jakarta', 'en_US')) + 900;
        $email_user = $this->request->getJsonVar('email');
        $check_email = $this->user_model->where('email', $email_user)->get()->getRowObject();
        $token_request = base64_encode($time_now) . '-' . str_shuffle($permitted_chars) . '/' . base64_encode($email_user);
        $body_email = '<h1>Reset Password Akun Vocasia</h1><br>
        <p>Halo sobat vocasia,terima kasih telah menggunakan vocasia berikut adalah akses reset password kamu ! link : <a href = "https://vocasia-dev.vercel.app/change-password?token=' . $token_request . '">reset password</a></p>';
        try {
            if ($check_email) {
                $email->setFrom('backendvocasia@gmail.com', 'Vocasia ID');
                $email->setTo($check_email->email);
                $email->setSubject('Reset Password Akun Vocasia');
                $email->setMessage($body_email);
                $email->send();
                return $this->respond('Send !');
            } else {
                return $this->failNotFound('Email Not Found !');
            }
        } catch (\Exception $e) {
            return $this->fail('Bad Request !');
        }
    }

    public function reset_password()
    {
        $form_data = $this->request->getJSON();
        $decode_token = explode('/', $form_data->token);
        $email = base64_decode($decode_token[1]);
        $rules_validation = [
            'token' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'token required'
                ]
            ],
            'password' => [
                'rules' => 'required|min_length[8]',
                'errors' => [
                    'required' => 'password required',
                    'min_length' => 'password less than 8'
                ]
            ],
            'confirm_password' => [
                'rules' => 'required|min_length[8]|matches[password]',
                'error' => [
                    'required' => 'confirm password required',
                    'min_length' => 'password less than 8',
                    'matches' => 'password not match '
                ]
            ]

        ];

        if (!$this->validate($rules_validation)) {
            return $this->respond([
                'status' => 403,
                'error' => false,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {
            $this->_check_expired($form_data->token);
            $find_user = $this->user_model->where('email', $email)->get()->getRowObject();

            if ($find_user) {
                $data = array(
                    'password' => sha1($form_data->password)
                );
                $this->user_model->update($find_user->id, $data);
                return $this->respondUpdated(response_update());
            } else {
                return $this->failNotFound();
            }
        }
    }

    private function _check_expired($token)
    {
        $time = new Time();
        $decode_token = explode('-', $token);
        $time_create = base64_decode($decode_token[0]);
        $time_now = strtotime($time->now('Asia/Jakarta', 'en_US'));
        if ($time_create > $time_now) {
            return;
        } else {
            return $this->failForbidden('token expired');
            die;
        }
    }
}
