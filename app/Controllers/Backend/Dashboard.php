<?php

namespace App\Controllers\Backend;

use App\Controllers\Backend\BackendController;

class Dashboard extends BackendController
{
    protected $format = 'json';

    public function index()
    {
        $year = $this->request->getVar('year');
        $data_courses = $this->model_course->get_count_course();
        $data_courses_active = $this->model_course->get_count_course_active();
        $data_lessons = $this->model_lesson->get_count_lesson();
        $data_enrols = $this->model_enrol->get_count_enrol();
        $data_users = $this->model_users->get_count_user();

        $jan = $this->model_enrol->count_new_enrol('1', $year);
        $feb = $this->model_enrol->count_new_enrol('2', $year);
        $mar = $this->model_enrol->count_new_enrol('3', $year);
        $apr = $this->model_enrol->count_new_enrol('4', $year);
        $mei = $this->model_enrol->count_new_enrol('5', $year);
        $jun = $this->model_enrol->count_new_enrol('6', $year);
        $jul = $this->model_enrol->count_new_enrol('7', $year);
        $agu = $this->model_enrol->count_new_enrol('8', $year);
        $sep = $this->model_enrol->count_new_enrol('9', $year);
        $okt = $this->model_enrol->count_new_enrol('10', $year);
        $nov = $this->model_enrol->count_new_enrol('11', $year);
        $des = $this->model_enrol->count_new_enrol('12', $year);
        
        $data_instructor = $this->model_users->get_data_instructor();
        $data = array();
        if ($data_instructor) {
            foreach ($data_instructor as $instructor) {
                $data[] = [
                    "id" => $instructor->id,
                    "fullname" => $instructor->first_name . ' ' . $instructor->last_name,
                    "email" => $instructor->email,
                    "is_instructor" => $instructor->is_instructor,
                    "courses" => $instructor->course_id,
                    "title" => $instructor->title,
                    "instructor_revenue" => $this->model_users->count_revenue($instructor->course_id),
    
                ];
            }
        }else {
            $data = $this->failNotFound();
        }

        return $this->respond([
            'status' => 201,
            'error' => false,
            'data' => [
                'courses' => $data_courses,
                'courses-active' => $data_courses_active,
                'lessons' => $data_lessons,
                'enrols' => $data_enrols,
                'users' => $data_users,
                'new-users' => [
                    'jan' => $jan,
                    'feb' => $feb,
                    'mar' => $mar,
                    'apr' => $apr,
                    'mei' => $mei,
                    'jun' => $jun,
                    'jul' => $jul,
                    'agu' => $agu,
                    'sep' => $sep,
                    'okt' => $okt,
                    'nov' => $nov,
                    'des' => $des
                ],
                'data-instructor' => $data
            ]
        ]);
    }
}
