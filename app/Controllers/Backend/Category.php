<?php

namespace App\Controllers\Backend;

use App\Controllers\Backend\BackendController;

class Category extends BackendController
{
    protected $format = 'json';

    public function index()
    {
        $data_category = $this->model_category->get_category();
        if ($data_category) {
            foreach ($data_category as $category) {
                $data[] = [
                    "id" => $category['id'],
                    "code_category" => $category['code_category'],
                    "name_category" => $category['name_category'],
                    "parent_category" => $category['parent_category'],
                    "slug_category" => $category['slug_category'],
                    "font_awesome_class" => $category['font_awesome_class'],
                    "thumbnail" => $this->model_category->get_thumbnail($category['id']),
                    "create_at" => $category['create_at'],
                    "update_at" => $category['update_at']
                ];
            }
            return $this->respond(get_response($data));
        } else {
            return $this->failNotFound();
        }
    }

    public function show($params = null)
    {
        $data_category = $this->model_category->get_category($params);
        if ($data_category) {
            $data = [
                "id" => $data_category['id'],
                "code_category" => $data_category['code_category'],
                "name_category" => $data_category['name_category'],
                "parent_category" => $data_category['parent_category'],
                "slug_category" => $data_category['slug_category'],
                "font_awesome_class" => $data_category['font_awesome_class'],
                "thumbnail" => $this->model_category->get_thumbnail($data_category['id']),
                "create_at" => $data_category['create_at'],
                "update_at" => $data_category['update_at']
            ];
            return $this->respond(get_response($data));
        } else {
            return $this->failNotFound();
        }
    }

    public function create($id_category = null)
    {
        $rules = $this->model_category->validationRules;
        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        } else {
            $thumbnail = $this->request->getFile('thumbnail');
            $data_category = $this->request->getVar();
            if (!is_null($id_category)) {
                $this->model_category->update($id_category, $data_category);
                return $this->respond(response_update());
                if (!empty($thumbnail)) {
                    $this->thumbnail($id_category, $thumbnail);
                } else {
                    return $this->respond(response_update());
                }
            } else {
                $thumbnail = $this->request->getFile('thumbnail');
                $slug_category = url_title($data_category['name_category']);
                $data_category['slug_category'] = $slug_category;
                $this->model_category->insert($data_category);
                $get_kategori_id = $this->model_category->id_category();
                $this->thumbnail($get_kategori_id, $thumbnail);
                return $this->respondCreated(response_create());
            }
        }
    }

    // public function update($params = null)
    // {
    //     $data_by_id = $this->model_category->find($params);

    //     if ($data_by_id) {
    //         $data_category = $this->request->getJSON();
    //         if ($this->request->getVar("name_category")) {
    //             $slug_category = url_title($data_category->name_category);

    //             $data_category->slug_category = $slug_category;
    //         }

    //         $this->model_category->protect(false)->update($params, $data_category);
    //         return $this->respondCreated(response_update());
    //     } else {
    //         return $this->failNotFound();
    //     }
    // }

    public function delete($params = null)
    {
        $data_category = $this->model_category->find($params);
        if ($data_category) {
            $this->model_category->delete($params);

            return $this->respondDeleted(response_delete());
        } else {
            return $this->failNotFound();
        }
    }

    public function thumbnail($params = null, $thumbnail = null)
    {
        $data_category = $this->model_category->find($params);
        $rules = [
            'thumbnail' => 'max_size[thumbnail,2048]|is_image[thumbnail]'
        ];
        if (!$this->validate($rules)) {
            return $this->fail('Failed To Upload Image Please Try Again');
        } else {
            if ($data_category) {
                $path = "uploads/category_thumbnail";
                if (!file_exists($path)) {
                    mkdir($path);
                }
                $data_thumbnail = $thumbnail;
                $name = "category_thumbnail_default_$params.jpg";
                $path_photo = $path . '/' . $name;
                if (file_exists($path_photo)) {
                    unlink('uploads/category_thumbnail/' . $name);
                }
                $data_thumbnail->move('uploads/category_thumbnail/', $name);
                // $this->model_category->update($params, $data);

                return $this->respondCreated(response_create());
            } else {
                return $this->failNotFound();
            }
        }
    }
}
