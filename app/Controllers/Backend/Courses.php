<?php

namespace App\Controllers\Backend;

use App\Controllers\Backend\BackendController;
use Exception;
use PhpParser\JsonDecoder;

class Courses extends BackendController
{
    protected $format = 'json';

    public function index()
    {
        if (!is_null($this->request->getVar('page')) && !is_null($this->request->getVar('limit'))) {
            $page = $this->request->getVar('page');
            $limit = $this->request->getVar('limit');
            $pagging = $this->pagging($page, $limit);
            return $this->respond(response_pagging($pagging['total_page'], $pagging['data']));
        }

        $course_list = $this->model_course->get_course_list();
        if ($course_list) {
            foreach ($course_list as $courses) {
                $total_lesson = $this->model_lesson->get_lesson($courses->id);
                $total_section = $this->model_section->count_section($courses->id);
                $data[] = [
                    'title' => $courses->title,
                    'create_at' => $courses->create_at,
                    'instructor_name' => $courses->first_name . ' ' . $courses->last_name,
                    'name_category' => $courses->name_category,
                    'lesson' => empty($this->model_lesson->get_lesson($courses->id)) ? 0 : $total_lesson->total_lesson,
                    'section' => empty($this->model_section->count_section($courses->id)) ? 0 : $total_section->total_section,
                    'count_enrol' => empty($this->model_enrol->get_count_enrols_courses($courses->id)) ? 0 : $this->model_enrol->get_count_enrols_courses($courses->id),
                    'status_course' => $courses->status_course,
                    'price' => $courses->price,
                    "id" => $courses->id
                ];
            }
            return $this->respond(get_response($data));
        } else {
            return $this->failNotFound();
        }
    }

    public function show_detail($id_course)
    {

        if ($this->request->getVar('curriculum')) {
            $data = array();
            $course_id = $this->request->getVar('curriculum');
            $section_title = $this->model_course->get_section_title($course_id);
            foreach ($section_title as $st) {
                $course_by_section  = $this->model_lesson->lesson_title_from_section($st->id);
                $data[] = [
                    'id_section' => $st->id,
                    'title' => $st->title,
                    'data_lesson' => $course_by_section
                ];
            }
            return $this->respond(get_response($data));
        }

        if ($this->request->getVar('bonus')) {
            $course_id = $this->request->getVar('bonus');
            $get_bonus_course = $this->model_bonus_course->where('course_id')->get()->getResultObject();
            return $this->respond(get_response($get_bonus_course));
        } else {
            $get_detail_course = $this->model_course->find($id_course);
            $get_detail_course['thumbnail'] = $this->model_course->get_thumbnail($id_course);

            return $this->respond([
                'status' => 200,
                'error' => false,
                'data' => $get_detail_course,
            ]);
        }
    }

    public function create()
    {
        $data_course = $this->request->getVar();
        $thumbnail = $this->request->getFile('thumbnail');
        if (is_null($data_course)) {
            throw new Exception('Data Request Not Found! Failed To Create Please Try Again');
        }
        try {
            $this->model_course->protect(false)->insert($data_course);
            $id_data = $this->model_course->get_id_course();
            $this->thumbnail($id_data, $thumbnail);
            return $this->respondCreated(response_create());
        } catch (Exception $e) {
            return $this->respondNoContent($e->getMessage());
        }
    }


    public function update($id_course = null)
    {
        $data_course = $this->request->getVar();
        $thumbnail = $this->request->getFile('thumbnail');
        $course_list_by_id = $this->model_course->find($id_course);
        if (is_null($data_course) || is_null($course_list_by_id)) {
            throw new Exception('Data Request Not Found Or Id Not Found!! Failed To Update Please Try Again');
        }
        try {
            $this->model_course->protect(false)->update($id_course, $data_course);
            $this->thumbnail($id_course, $thumbnail);
            return $this->respondUpdated(response_update());
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function delete($id_course = null)
    {
        $course_list_by_id = $this->model_course->find($id_course);
        if (is_null($course_list_by_id)) {
            return $this->failNotFound();
        }
        $this->model_course->delete($id_course);
        return $this->respondDeleted(response_delete());
    }

    public function thumbnail($id_course = null, $thumbnail = null)
    {
        $data_course = $this->model_course->find($id_course);
        $rules = [
            'thumbnail' => 'max_size[thumbnail,2048]|is_image[thumbnail]'
        ];
        if (!$this->validate($rules)) {
            return $this->fail('Failed To Upload Image Please Try Again');
        } else {
            if ($data_course) {
                $path = 'uploads/courses_thumbnail';
                if (!file_exists($path)) {
                    mkdir($path);
                }
                $id_course_thumbnail = $data_course['id'];
                $path_folder = `uploads/courses_thumbnail/course_thumbnail_default_$id_course_thumbnail.jpg`;
                $data_thumbnail = $thumbnail;
                $name = "course_thumbnail_default_$id_course_thumbnail.jpg";
                if (file_exists($path_folder)) {
                    unlink('uploads/courses_thumbnail/' . $name);
                }
                $data_thumbnail->move('uploads/courses_thumbnail/', $name);
                // $this->model_course->update($id_course, $data);
                return $this->respondCreated(response_create());
            } else {
                return $this->failNotFound();
            }
        }
    }

    public function pagging($page, $offset)
    {
        $start_index = ($page > 1) ? ($page * $offset) - $offset : 0; // hitung page saat ini
        $count_data = $this->model_course->get_count_course(); // hitung total data ini akan mengembalikan angka
        $total_pages = ceil($count_data / $offset); //perhitungan dari jumlah data yg dihitung dibagi dengan batas data yg ditentukan
        $get_pagging_data = $this->model_course->get_pagging_data($offset, $start_index); //query berdasarkan data per halaman

        foreach ($get_pagging_data as $courses) {
            $total_lesson = $this->model_lesson->get_lesson($courses->id);
            $total_section = $this->model_section->count_section($courses->id);
            $data[] = [
                'title' => $courses->title,
                'create_at' => $courses->create_at,
                'instructor_name' => $courses->first_name . ' ' . $courses->last_name,
                'name_category' => $courses->name_category,
                'lesson' => empty($this->model_lesson->get_lesson($courses->id)) ? 0 : $total_lesson->total_lesson,
                'section' => empty($this->model_section->count_section($courses->id)) ? 0 : $total_section->total_section,
                'count_enrol' => empty($this->model_enrol->get_count_enrols_courses($courses->id)) ? 0 : $this->model_enrol->get_count_enrols_courses($courses->id),
                'status_course' => $courses->status_course,
                'price' => $courses->price,
                "id" => $courses->id,
                "instructor_revenue" => $courses->instructor_revenue,
                "is_prakerja" => $courses->is_prakerja
            ];
        }

        $return_data = [
            'total_page' => $total_pages,
            'data' => $data
        ];
        return $return_data;
    }

    public function info_courses()
    {
        $data_courses_active = $this->model_course->get_count_course_active();
        $data_courses_pending = $this->model_course->get_count_course_pending();
        $data_courses_free = $this->model_course->get_count_course_free();
        $data_courses_paid = $this->model_course->get_count_course_paid();

        return $this->respond([
            'status' => 201,
            'error' => false,
            'data' => [
                'courses-active' => $data_courses_active,
                'courses-pending' => $data_courses_pending,
                'courses-free' => $data_courses_free,
                'courses-paid' => $data_courses_paid,
            ]
        ]);
    }
    public function get_all_section($id_course)
    {
        $section = $this->model_section->all_section($id_course);
        if ($id_course) {
            return $this->respond(get_response($section));
        } else {
            return $this->failNotFound();
        }
    }

    public function add_quiz($id_course)
    {
        $course = $this->model_course->find($id_course);
        $section = $this->model_section->get_section_quiz($this->request->getVar("section_id"), $id_course);
        if ($course && $section) {
            $data = $this->request->getJSON();
            $data->course_id = $id_course;
            $data->lesson_type = "quiz";
            $data->duration = "00:00:00";

            $this->model_lesson->insert($data);
            return $this->respondCreated(response_create());
        } else {
            return $this->failNotFound();
        }
    }

    public function sortir_bab()
    {
        $sectionJSON = $this->request->getVar("itemjson");

        foreach ($sectionJSON as $key => $value) {
            $updater = [
                'order' => intval($key) + 1
            ];

            $this->model_section->update($value, $updater);
        }
        return $this->respondCreated(response_create());
    }

    public function edit_section($id_course, $id_section)
    {

        $rules = ['title' => 'required'];

        if (!$this->validate($rules)) {
            return $this->respond([
                'status' => 403,
                'error' => true,
                'data' => [
                    'message' => $this->validator->getErrors()
                ]
            ], 403);
        } else {

            $data_course = $this->model_section->where('course_id', $id_course);

            if (empty($data_course)) {
                return $this->failNotFound();
            } else {

                $data_section = $this->model_section->find($id_section);
                if (empty($data_section)) {
                    return $this->failNotFound();
                } else {

                    $value = $this->request->getJSON();
                    $data = ['title' => $value->title];

                    $this->model_section->update($id_section, $data);
                    return $this->respondUpdated(response_update());
                }
            }
        }
    }

    public function delete_section($id_course, $id_section)
    {

        $data_course = $this->model_section->where('course_id', $id_course);

        if (empty($data_course)) {
            return $this->failNotFound();
        } else {
            $data_section = $this->model_section->find($id_section);
            if (empty($data_section)) {
                return $this->failNotFound();
            } else {
                $this->model_section->delete_section($id_course, $id_section);
                return $this->respondDeleted(response_delete());
            }
        }
    }
    public function add_section($course_id)
    {
        $section_request = $this->request->getJSON();
        $this->model_section->insert([
            'course_id' => $course_id,
            'title' => $section_request->title,
        ]);
        return $this->respondCreated(response_create());
    }

    public function add_lesson($course_id)
    {
        $lesson_request = $this->request->getJSON();
        $this->model_lesson->insert([
            'title' => $lesson_request->title,
            'course_id' => $course_id,
            'section' => $lesson_request->section_id,
            'lesson_type' => $lesson_request->lesson_type,
            'summary' => $lesson_request->summary,
        ]);
        return $this->respondCreated(response_create());
    }
}
