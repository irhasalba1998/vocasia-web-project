<?php

use CodeIgniter\I18n\Time;

function generate_humanize_timestamps($time)
{
    $date_stamps = Time::createFromTimestamp($time, 'Asia/Jakarta', 'en_US');
    $get_date = $date_stamps->toDateTimeString();
    $exp = explode('-', $get_date);
    $date = substr($exp[2], 0, 2);
    $month_in_bahasa = [
        "01" => 'Januari',
        "02" => 'Februari',
        "03" => 'Maret',
        "04" => 'April',
        "05" => 'Mei',
        "06" => 'Juni',
        "07" => 'Juli',
        "08" => 'Agustus',
        "09" => 'September',
        "10" => 'Oktober',
        "11" => 'November',
        "12" => 'Desember'

    ];
    $formated_humanize = $date . ' ' . $month_in_bahasa[$exp[1]] . ' ' . $exp[0];
    return $formated_humanize;
}
