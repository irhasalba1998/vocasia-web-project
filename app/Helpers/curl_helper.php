<?php

use Config\Services;

function curlRequest($data)
{

    $secret_key = 'Basic ' . base64_encode(Services::getMidtransServerKey());
    $client = \Config\Services::curlrequest();
    $url_request = $client->setBody(json_encode($data))->request('post', 'https://api.sandbox.midtrans.com/v2/charge', array(
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => $secret_key,
            'Content-Type' => 'application/json'
        ]
    ));
    $response = json_decode($url_request->getBody());
    return $response;
}
