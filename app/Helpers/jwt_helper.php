<?php

use Config\Services;
use Firebase\JWT\JWT;
use App\Models\UsersModel as User;

function verify_request($token, $user_id)
{
    $secret_key = Services::getSecretKey();
    $decode_token = explode(' ', $token);

    $decoded_token = JWT::decode($decode_token[1], $secret_key, array('HS256'));
    if (base64_decode($decoded_token->id_user) == $user_id) {
        return true;
    } else {
        return false;
    }
}

function check_email_token($token, $email)
{
    $secret_key = Services::getSecretKey();
    $explode_token = explode(' ', $token);
    $decoded_token = JWT::decode($explode_token[1], $secret_key, array('HS256'));
    $user_model = new User();
    $find_user = $user_model->where(['id' => base64_decode($decoded_token->id_user), 'email' => $decoded_token->email])->first();
    if ($find_user['email'] == $email) {
        return $find_user['email'];
    } else {
        return false;
    }
}
