<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Models\UsersModel as User;

class QnaModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'qna_thread';
    protected $primaryKey       = 'id_qna';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['sender', 'quest', 'id_course', 'up', 'user_id_up', 'status'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'create_at';
    protected $updatedField  = 'update_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function get_qna($course_id)
    {
        return $this->db->table('qna_thread a')->select('a.*,b.first_name,b.last_name')->join('users b', 'b.id = a.sender')->where('a.id_course', $course_id)->get()->getResult();
    }

    public function get_comment($id_qna)
    {
        $user = new User();
        $data_array = array();
        $data_comment = $this->db->table('qna_replies a')->select('a.*,b.first_name,b.last_name')->join('users b', 'b.id = a.sender')->where('a.id_qna', $id_qna)->get()->getResult();
        foreach ($data_comment as $dc) {
            $data_array[] = [
                'sender' => $dc->sender,
                'username' => $dc->first_name . ' ' . $dc->last_name,
                'test_rep' => $dc->text_rep,
                'foto_profile' => $user->get_foto_profile($dc->sender)

            ];
        }

        return $data_array;
    }
}
