<?php

namespace App\Models;

use CodeIgniter\Model;

class SectionModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'section';
    protected $primaryKey           = 'id';
    protected $useAutoIncrement     = true;
    protected $insertID             = 0;
    protected $returnType           = 'array';
    protected $useSoftDeletes       = false;
    protected $protectFields        = true;
    protected $allowedFields        = ['course_id','title','order'];

    // Dates
    protected $useTimestamps        = false;
    protected $dateFormat           = 'datetime';
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks       = true;
    protected $beforeInsert         = [];
    protected $afterInsert          = [];
    protected $beforeUpdate         = [];
    protected $afterUpdate          = [];
    protected $beforeFind           = [];
    protected $afterFind            = [];
    protected $beforeDelete         = [];
    protected $afterDelete          = [];

    public function get_section($id_course = null)
    {
        return $this->db->table('section')->select("
                section.title as title_section")
            ->join('courses', 'courses.id = section.course_id')
            ->where('section.course_id', $id_course)
            ->get()
            ->getResultArray();
    }
    
    public function count_section($course_id)
    {
        return $this->db->table('section')->selectCount('course_id', 'total_section')->where('course_id', $course_id)->orderBy('course_id')->get()->getFirstRow();
    }

    public function get_curriculum($course_id)
    {
        $get_curiculum = $this->db->table('section a')->select('a.title as section_title ,a.order,b.title as lesson_title')->join('lesson b', 'b.section_id = a.id')->where('a.course_id', $course_id)->get()->getResultObject();
        $data_array = array();
        foreach ($get_curiculum as $key => $cr) {
            $data_array[$key] = [
                'section_title' => $cr->section_title,
                'lesson_title' => $cr->lesson_title,
            ];
        }
        return $data_array;;
    }

    public function edit_section($data, $id_section){

        $value = ['title' => $data];
        return $this->db->table('section')->where('id', $id_section)->update($value);
    }

    public function delete_section($id_course, $id_section){

        $this->db->table('section')->where('id', $id_section)->delete();

        $data_course = $this->db->table('courses')->where('id', $id_course)->get()->getRow();   
        $section = $data_course->section;
        $previous_sections = json_decode($section);

        if (sizeof($previous_sections) > 0) {
            $new_section = array();
                for ($i = 0; $i < sizeof($previous_sections); $i++) {
                    if ($previous_sections[$i] != $id_section) {
                        array_push($new_section, $previous_sections[$i]);
                    }
                }
            $updater['section'] = json_encode($new_section);
            return $this->db->table('courses')->where('id', $id_course)->update($updater);
        }
    }
    
    public function all_section($id_course = null)
    {
        return $this->db->table('section')->select("
                section.id, section.title as title_section")
                ->join('courses', 'courses.id = section.course_id')
                ->where('section.course_id', $id_course)
                ->get()
                ->getResultArray();
    }

    public function get_section_quiz($id = null ,$id_course = null)
    {
        return $this->db->table('section')->select("
                section.title as title_section")
                ->join('courses', 'courses.id = section.course_id')
                ->where('section.id', $id)
                ->where('section.course_id', $id_course)
                ->get()
                ->getRowArray();
    }
}
