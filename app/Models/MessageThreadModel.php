<?php

namespace App\Models;

use CodeIgniter\Model;

class MessageThreadModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'message_thread';
    protected $primaryKey       = 'message_thread_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['message_thread_code', 'sender', 'receiver', 'last_message_timestamp'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'int';
    protected $createdField  = 'last_message_timestamp';
    protected $updatedField  = '';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function check_message($sender, $receiver)
    {
        return $this->db->table($this->table)->where(['sender' => $sender, 'receiver' => $receiver])->get()->getResult();
    }
}
