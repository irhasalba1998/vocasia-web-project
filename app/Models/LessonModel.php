<?php

namespace App\Models;

use CodeIgniter\Model;

class LessonModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'lesson';
    protected $primaryKey           = 'id';
    protected $useAutoIncrement     = true;
    protected $insertID             = 0;
    protected $returnType           = 'array';
    protected $useSoftDeletes       = false;
    protected $protectFields        = true;
    protected $allowedFields        = ['title', 'duration', 'course_id', 'section_id', 'video_type', 'video_url', 'lesson_type', 'attachment', 'attachment_type', 'summary', 'is_skip', 'order', 'video_type_for_mobile', 'video_url_for_mobile', 'duration_for_mobile'];

    // Dates
    protected $useTimestamps        = true;
    protected $dateFormat           = 'int';
    protected $createdField         = 'create_at';
    protected $updatedField         = 'update_at';
    protected $deletedField         = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks       = true;
    protected $beforeInsert         = [];
    protected $afterInsert          = [];
    protected $beforeUpdate         = [];
    protected $afterUpdate          = [];
    protected $beforeFind           = [];
    protected $afterFind            = [];
    protected $beforeDelete         = [];
    protected $afterDelete          = [];

    public function get_list_lesson($id = null)
    {
        if (is_null($id)) {
            return $this->db->table('lesson')->selectCount('course_id', 'total_lesson')->where('course_id', $id)->groupBy('course_id')->get()->getResult();
        }
        return $this->db->table('lesson')->select("
                lesson.*")
            ->join('courses', 'courses.id = lesson.course_id')
            ->join('section', 'section.id = lesson.section_id')
            ->where('lesson.id', $id)
            ->get()
            ->getRow();
    }

    public function get_lesson($id_course = null)
    {
        $total_lesson = $this->db->table('lesson')->selectCount('course_id', 'total_lesson')->where('course_id', $id_course)->groupBy('course_id')->get()->getFirstRow();
        return $total_lesson;
    }

    public function get_count_lesson()
    {
        return $this->db->table('lesson')->select("
                lesson.*")
            ->join('courses', 'courses.id = lesson.course_id')
            ->join('section', 'section.id = lesson.section_id')
            ->countAllResults();
    }

    public function count_section($course_id)
    {
        return $this->db->table('lesson')->select('lesson.course_id')
            ->where('lesson.course_id', $course_id)->countAllResults();
    }

    public function lesson_title_from_section($id_section)
    {
        return $this->db->table('lesson')->select('id as id_lesson,title as title_lesson')->where('section_id', $id_section)->get()->getResultObject();
    }

    public function get_id_lesson($id_course, $id_lesson)
    {

        return $this->db->table('lesson')->select('lesson.id')
            ->where('course_id', $id_course)
            ->where('id', $id_lesson)
            ->get()
            ->getRow();
    }

    public function get_type_video_lesson($course_id)
    {
        $lesson_type = $this->db->table('lesson')->select('video_type')->where('course_id', $course_id)->get()->getFirstRow();
        return $lesson_type->video_type;
    }
}
