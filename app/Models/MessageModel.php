<?php

namespace App\Models;

use CodeIgniter\Model;

class MessageModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'message';
    protected $primaryKey       = 'message_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['message_thread_code', 'message', 'sender', 'read_status', 'timestamp'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'int';
    protected $createdField  = 'timestamp';
    protected $updatedField  = '';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function get_receiver($receiver_id)
    {
        return $this->db->table('users')->select('first_name,last_name')->where('id', $receiver_id)->get()->getFirstRow();
    }

    public function get_last_message($messages_thread_code, $last_timestamp)
    {
        $data = array();
        $last_msg = $this->db->table($this->table)->select('message')->where(['message_thread_code' => $messages_thread_code, 'timestamp' => $last_timestamp])->orderBy('timestamp', 'DESC')->get()->getRow();
        return $last_msg;
    }
}
