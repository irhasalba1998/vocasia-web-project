## Rules Project Pengembangan Web Vocasia

### Structure Folders

- untuk struktur folder disini dibagi menjadi 2 yakni :
  - Backend : folder yang digunakan untuk menyimpan resources yang berhubungan dengan admin,usahakan semua inheritance/extends class nya berada di BackendController bukan di Base lagi
  - Frontend : folder yang digunakan untuk menampung resources yang berhubungan dengan non admin/end user,usahakan semua inheritance/extends juga di FrontendController bukan di base lagi

### Rules Code Development

- untuk peraturan daklam penulisan coding mengikuti saran dari kak jul dan mas iksan selebihnya dan juga kesepakatan yang telah dibuat kemarin

### Routing Rules
- group admin : untuk endpoint dengan request dashboard admin
- group homepage : untuk endpoint dengan request ke halaman selain admin tanpa membutuhkan login
- group users : untuk endpoint dengan request users yang mengharuskan login terlebih dahulu

### Helpers Project 
disini tim backend membuat beberapa helpers yang bertujuan untuk memudahkan proses coding
- curl_helper : digunakan untuk berkomunikasi dengan API midtrans jadi tinggal panggil satu helper saja agar lebih mudah
- generate_random_string : digunakan untuk otomatis generate token string yang dipakai untuk refreral atau yang lain
- jwt_helper : digunakan untuk memvalidasi jwt request, didalam jwt helper terdapat 2 fungsi sebagai berikut : 
                 - verify request : untuk memverifikasi request yang masuk apakah id token sama dengan id request
                 - check_email_token : untuk memverifikasi request email yang masuk apakah sesuai dengan email token
- parse_date_helper : digunakan untuk memparsing integer timestamp pada kolom create_at / update_at dari angka 1234450 menjadi tanggal umum seperti 10 januari 2022
- pusher_helper : digunakan untuk handle notifikasi dari pembayaran midtrans
- response_helper : digunakan untuk menata response yang dikeluarkan,helper ini sangat sering digunakan dalam project ini, teman teman bisa mengguanakan get_response untuk GET response atau response_create untuk POST response

### Controller Structure
terdapat 2 controller base untuk project ini :
- BackendController : untuk meload model atau helper yang berhubungan dengan fitur admin
- FrontendController : untuk meload model atau helper yang berhubungan dengan fitur selain admin


### Dokumen Hand Offer
google docs : <https://docs.google.com/document/d/1RH3-j4_AXPAEM9TZ9LeGPZLd36Dhu5r0x6JQia6o5-4/edit?usp=sharing>
